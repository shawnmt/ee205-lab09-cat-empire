///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 09a - Cat Empire!
///
/// @file cat.cpp
/// @version 1.0
///
/// Exports data about all cats
///
/// @author Shawn Tamashiro <shawnmt@hawaii.edu>
/// @brief  Lab 09a - Cat Empire! - EE 205 - Spr 2021
/// @date   30 Apr 2021
///////////////////////////////////////////////////////////////////////////////

#include <cassert>
#include <random>
#include <queue>
#include <cstdlib>

#define CAT_NAMES_FILE "names.txt"

#include "cat.hpp"

using namespace std;

Cat::Cat( const std::string newName ) {
	setName( newName );
}


void Cat::setName( const std::string newName ) {
	assert( newName.length() > 0 );

	name = newName;
}


std::vector<std::string> Cat::names;


void Cat::initNames() {
	names.clear();
	cout << "Loading... ";

	ifstream file( CAT_NAMES_FILE );
	string line;
	while (getline(file, line)) names.push_back(line);

	cout << to_string( names.size() ) << " names." << endl;
}


Cat* Cat::makeCat() {
	// Get a random cat from names
	auto nameIndex = rand() % names.size();
	auto name = names[nameIndex];

	// Remove the cat (by index) from names
	erase( names, names[nameIndex] );

	return new Cat( name );
}

bool CatEmpire::empty(){
   if( topCat == nullptr ){
      return true;
   }
   else{
      return false;
   }
}

void CatEmpire::addCat(Cat* newCat){
   newCat->left =nullptr;
   newCat->right =nullptr;
   if(topCat == nullptr){
      topCat = newCat;
      return;
   }
   addCat(topCat, newCat);
}

void CatEmpire::addCat( Cat* atCat, Cat* newCat ){
      if(atCat->name > newCat->name){
         if(atCat->left == nullptr){
            atCat->left = newCat;
         }
         else{
            addCat( atCat->left, newCat);
         }
      }
      if(atCat->name < newCat->name){
         if(atCat->right == nullptr){
            atCat->right = newCat;
         }
         else{
            addCat( atCat->right, newCat);
         }
      }
}

void CatEmpire::catFamilyTree() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

	dfsInorderReverse( topCat, 1 );
}

void CatEmpire::dfsInorderReverse( Cat* atCat, int depth ) const {
   if( atCat == nullptr )
      return;

   dfsInorderReverse( atCat->right, depth + 1 );

   cout << string( 6 * (depth-1), ' ' ) << atCat->name;
   
   if( atCat->left == nullptr && atCat->right == nullptr )
      cout << endl;
   if( atCat->left != nullptr && atCat->right != nullptr )
      cout << '<' << endl;
   if( atCat->left != nullptr && atCat->right == nullptr )
      cout << '\\' << endl;
   if( atCat->left == nullptr && atCat->right != nullptr )
      cout << '/' << endl;

   dfsInorderReverse( atCat->left, depth + 1 );
}

void CatEmpire::catList() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

   dfsInorder( topCat );
}

void CatEmpire::dfsInorder( Cat* atCat ) const {
   if( atCat == nullptr )
      return;

   dfsInorder( atCat->left );
   cout << atCat->name << endl;
   dfsInorder( atCat->right );
}

void CatEmpire::catBegat() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

   dfsPreorder( topCat );
}

void CatEmpire::dfsPreorder( Cat* atCat ) const {
   if( atCat == nullptr )
      return;

   if( atCat->left != nullptr && atCat->right != nullptr )
      cout << atCat->name << " begat " << atCat->left->name << " and " << atCat->right->name << endl;
   if( atCat->left != nullptr && atCat->right == nullptr )
      cout << atCat->name << " begat " << atCat->left->name << endl;
   if( atCat->left == nullptr && atCat->right != nullptr )
      cout << atCat->name << " begat " << atCat->right->name << endl;

   dfsPreorder( atCat->left );
   dfsPreorder( atCat->right );
}

void CatEmpire::catGenerations() const {

   int gen = 1;
   queue<Cat*> catQueue;
   catQueue.push( topCat );
   catQueue.push( nullptr );

   getEnglishSuffix( gen );
   cout << endl;

   while( !catQueue.empty() ) {
      Cat* cat = catQueue.front();   //cats infront of queue
      catQueue.pop();

      if( cat == nullptr ) {
         gen++;
         catQueue.push( nullptr );
         if( catQueue.front() == nullptr ) { //end of queue
            cout << endl;
            return;
         }
         else { 
            cout << endl; 
            getEnglishSuffix( gen );
            cout << endl;
            continue;
         }
      }

      if( cat->left != nullptr )
         catQueue.push( cat->left );
      if( cat->right != nullptr )
         catQueue.push( cat->right );

      cout << " " << cat->name;
   }
   cout << endl;
}


void CatEmpire::getEnglishSuffix( int n) const {
   if(n%10 ==1 && n != 11){
      cout << n << "st ";
   }else if(n%10 ==2 && n != 12){
      cout << n << "nd ";
   }else if(n%10 ==3 && n !=13){
      cout << n << "rd ";
   }else{
      cout << n << "th ";
   }

}


