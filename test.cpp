///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 09a - Cat Empire!
///
/// @file test.cpp
/// @version 1.0
///
/// unit test
///
/// @author Shawn Tamashiro <shawnmt@hawaii.edu>
/// @brief  Lab 09a - Cat Empire! - EE 205 - Spr 2021
/// @date   30 Apr 2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>
#include <unistd.h>

#include "cat.hpp"

const int NUMBER_OF_CATS = 20;

using namespace std;

int main() {
   cout << "TESTING TESTING!!!" <<endl;
   usleep(3000000);
   cout << "\nWe will begin testing the Cat Empire" << endl;
   usleep(3000000);
   cout << "\nTesting begins in:" << endl;
   usleep(1000000);
   cout << "\n3" << endl;
   usleep(1000000);
   cout << "\n2" << endl;
   usleep(1000000);
   cout<< "\n1\n" << endl;
   usleep(1000000);
   
   CatEmpire cat;

   //empty test
   cout << "First we test if the Empire is empty.\n" << endl;
   cout << "Is the Empire empty?" << boolalpha << cat.empty() << endl;

   //addcat test
   cout << "\nLets add one cat!" << endl;
   cat.addCat( new Cat("Shawn") );
   cout << "Added cat Shawn" << endl;
   cout <<"\nNow is the Empire empty?" << boolalpha << cat.empty() << "\n" <<endl;

   //suffic test
   cout << "Lets test our suffixes.\n" << endl;
   for(int s = 1; s < 33; s++){
      cat.getEnglishSuffix( s );
      cout << "Generation" << endl;
   }

   //family tree test
   cout << "Now lets test if we can build a family tree!" << endl;
   Cat::initNames();

   for( int i = 0 ; i < NUMBER_OF_CATS ; i++ ) {
		Cat* newCat = Cat::makeCat();

		cat.addCat( newCat );
	}

	cout << "Print a family tree of " << NUMBER_OF_CATS << " cats" << endl;

	cat.catFamilyTree();

} // main()

